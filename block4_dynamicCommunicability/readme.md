### Course material for block 3 Dynamic Communicability.

Tutors, please upload here only the final material before the class.

---------
### NOTES

- In order to run the last part of the notebook in this block, which is a practical example of this new framework to analyse connectivity for neuroimaging data, you will need the results (the Jaccobian matrices) created in the notebook *pyMOU\_EC\_Estimation.ipynb* in the *block 2* of the morning session. These results, file *J_mod.npy*, should have been saved in a folder named *model_param/*.