### Course material for block 2 Effective Connectivity.

This part starts with the notebook *pyMOU\_Simulation\_Estimation.ipynb* to use the pyMOU package to simulate activity for a multivariate Ornstein-Uhlenbeck (MOU) process -network with given connectivity- and then estimate the connectivity from the observed activity.

Then we apply the same estimation procedure in the notebook *pyMOU\_EC\_Estimation.ipynb* to extract MOU-EC from the data in the folder 'movie_data/'.

---------
### NOTES

