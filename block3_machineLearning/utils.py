import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import ShuffleSplit, GroupShuffleSplit
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.patches import Patch

def cohens_d(group1, group2):
    """
    Calculated effect size for the difference between two groups as Cohen's d.
    This method assumes that the two groups are independent (unpaired) and that the population variances are equal.
    
    PARAMETERS
    ----------
    group1 : 1D list or np.array
        observations from group1
    group2 : 1D list or np.array
        observations from group1
    
    RETURNS
    -------
    cohens_d: float
    """
    mean1 = np.mean(group1)
    mean2 = np.mean(group2)
    std1 = np.std(group1)
    std2 = np.std(group2)
    count1 = len(group1)
    count2 = len(group2)
    dof = (count1 + count2 - 2)
    pooled_std = np.sqrt(((count1 - 1) * std1 ** 2 + (count2 - 1) * std2 ** 2) / dof)
    cohens_d = abs(mean1 - mean2) / pooled_std
    return cohens_d

def pred_model_example(X, y):
    """
    This is an example of a predictive model.
    The function wraps definition, training and test of a logistic regression classifier.
    
    PARAMETERS
    ----------
    X: 2D array
        design matrix with samples on rows and features on columns
    y: 1D array
        labels to be predicted
    
    RETURNS
    -------
    scores: 1D array
        prediction accuracy on each CV fold
    """
    model = LogisticRegression(solver='lbfgs')
    folds = 5
    kf = KFold(n_splits=folds, shuffle=True)

    score = np.zeros([folds])
    i = 0  # counter for repetitions
    for train_idx, test_idx in kf.split(X):  # cross-validation
        model.fit(X[train_idx, :], y[train_idx])
        score[i] = model.score(X[test_idx, :], y[test_idx])
        i+=1
    return score

def plot_cv_indices(cv, X, y, group, ax, n_splits, lw=10):
    """From sklearn docs. Create a sample plot for indices of a cross-validation object."""

    cmap_cv = plt.cm.coolwarm
    cmap_data = plt.cm.Paired
    # Generate the training/testing visualizations for each CV split
    for ii, (tr, tt) in enumerate(cv.split(X=X, y=y, groups=group)):
        # Fill in indices with the training/test groups
        indices = np.array([np.nan] * len(X))
        indices[tt] = 1
        indices[tr] = 0

        # Visualize the results
        ax.scatter(range(len(indices)), [ii + .5] * len(indices),
                   c=indices, marker='_', lw=lw, cmap=cmap_cv,
                   vmin=-.2, vmax=1.2)

    # Plot the data classes and groups at the end
    ax.scatter(range(len(X)), [ii + 1.5] * len(X),
               c=y, marker='_', lw=lw, cmap=cmap_data)

    ax.scatter(range(len(X)), [ii + 2.5] * len(X),
               c=group, marker='_', lw=lw, cmap=cmap_data)

    # Formatting
    ax.set_title('{}'.format(type(cv).__name__), fontsize=15)
    return ax

def plot_cv_objects(n_points=100, percentiles_classes=[.5, .5], n_folds=4, cvs=[ShuffleSplit, GroupShuffleSplit]):
    """plot the behavior of different CV classes with example data."""
    # Generate the class/group data
    X = np.random.randn(100, 10)
    y = np.hstack([[ii] * int(100 * perc)
                   for ii, perc in enumerate(percentiles_classes)])
    # Evenly spaced groups repeated once
    groups = np.hstack([[ii] * 10 for ii in range(10)])

    cmap_cv = plt.cm.coolwarm

    fig, ax = plt.subplots(figsize=(16, 6), nrows=1, ncols=2, sharey=True)
    for i, cv in enumerate(cvs):
        this_cv = cv(n_splits=n_folds)
        plot_cv_indices(this_cv, X, y, groups, ax[i], n_folds)

        if i==3:
            ax[i].legend([Patch(color=cmap_cv(.8)), Patch(color=cmap_cv(.02))],
                      ['Testing set', 'Training set'], loc=(1.02, .8))
        yticklabels = list(range(n_folds)) + ['class', 'group']
        ax[i].set(yticks=np.arange(n_folds+2) + .5, yticklabels=yticklabels,
               xlabel='Sample index',
               ylim=[n_folds+2.2, -.2], xlim=[0, 100])
        # Make the legend fit
        plt.tight_layout()
        fig.subplots_adjust(right=.7)
    plt.show()
