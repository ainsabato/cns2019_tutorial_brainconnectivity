### Course material for block 3 Machine Learning.

ML.ipynb : this is the main notebook for this block. We'll use this for the class.

predictive_models_and_biomarkers.ipynb : this is a short notebook to motivate the use of predictive models and machine learning as an alternative to statistical inference whenever we are interested in the prediction of labels (as in the case of looking for biomarkers).

utils : here there are few simple functions that help keep the notebook clean.

---------
### NOTES



