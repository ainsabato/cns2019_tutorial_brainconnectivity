Repository for the tutorial:

### Model-based analysis of brain connectivity from neuroimaging data: estimation, analysis and classification.

held at the **[CNS\*2019 conference](https://www.cnsorg.org/cns-2019)**,

**When:** July 13, 2019. Morning: 09:30 – 12:40 / Afternoon: 14:30 – 17:40.

**Where:** Gran via de les Corts Catalans, 585. Historical Building of the University of Barcelona. Room B2.

![UB logo](Images/Logo_UB.png)

**Tutors:**

- [Andrea Insabato](https://andreainsabato.eu) (Center for Brain and Cognition, Pompeu Fabra University) 
- [Adrià Tauste](https://www.upf.edu/web/adria-tauste) (Barcelona Beta Brain Research Center)
- [Matthieu Gilson](https://matthieugilson.eu) (Center for Brain and Cognition, Pompeu Fabra University)
- [Gorka Zamora-López](http://www.zamora-lopez.xyz) (Center for Brain and Cognition, Pompeu Fabra University) 


----------------------

### NOTES:

- The material for the tutorial can be downloaded from the following online repository, [https://bitbucket.org/ainsabato/cns2019\_tutorial\_brainconnectivity/src/master/](https://bitbucket.org/ainsabato/cns2019_tutorial_brainconnectivity/src/master/).
- The tutorial will consist of both presentations and code showcases in Python using Jupyter Notebooks. See the file *CNS2019_brainConnectivity.yml* for the expected dependencies. 
- For attendees not very familiar with Python we recommend the [Anaconda Python distribution](https://www.anaconda.com/distribution/). In order to create a Python environment with the necessary packages for the tutorial follow either of the two instructions.

1. In the Anaconda Navigator (GUI), click on "Environments" --> Import button, and browse to the file *CNS2019_brainConnectivity.yml*.
2. From the command line, "cd" to the path with the file *CNS2019_brainConnectivity.yml* and type: `conda env create -f CNS2019_brainConnectivity.yml`






